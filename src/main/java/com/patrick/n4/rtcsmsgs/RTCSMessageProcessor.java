package com.patrick.n4.rtcsmsgs;

import com.patrick.n4.rtcsmsgs.common.XmlStreamUtil;
import com.patrick.n4.rtcsmsgs.fromrtcs.RTCSStartup;
import com.thoughtworks.xstream.XStream;

public class RTCSMessageProcessor {
    
    public static RTCSStartup getStartUp(String xml ){
        XStream xstream  = XmlStreamUtil.MakeXStream();

        return (RTCSStartup) xstream.fromXML(xml);
    }


    
}
