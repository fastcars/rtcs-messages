package com.patrick.n4.rtcsmsgs.common;

public class BulkRequestBody {
    private BulkRequestBodyContent bulkRequest;

    public BulkRequestBody(){
        this.bulkRequest = new BulkRequestBodyContent();
    }


    public BulkRequestBodyContent getBulkRequest() {
        return bulkRequest;
    }

    public void setBulkRequest(BulkRequestBodyContent bulkRequest) {
        this.bulkRequest = bulkRequest;
    }
}
