package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class BulkRequestBodyContent {
    @XStreamAsAttribute
    private String action;


    public BulkRequestBodyContent(){
        this.action = "TRK_PENDING";
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
