package com.patrick.n4.rtcsmsgs.common;

public enum CarrierType {
    TRUCK,
    VESSEL,
    RAIL
}
