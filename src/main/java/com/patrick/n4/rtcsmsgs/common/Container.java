package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@XStreamAlias("ctr")
public class Container {
    private String id;
    private int unitGkey;
    private Integer wiGkey;
    private String tranType;
    private int lengthType;
    private int height;
    private String isoCode;
    private ReeferContainer reefer;
    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"true", "false"})
    private boolean tank;

    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"true", "false"})
    private boolean flatRack;
    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"true", "false"})
    private boolean openTop;

    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"true", "false"})
    private boolean empty;
    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"true", "false"})
    private boolean pairable;
    private Integer pairedWith;
    private float grossWeight;
    private String commodity;
    private Hazards hazardous;
    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"true", "false"})
    private boolean oog;
    
    private Holds holds;
    private Position position;
    private String toLocation;
    private CarrierType obCarrierType;
    private String obCarrierRef;
    private String destination;
    private String lineOp;
    private String specialStow1;
    private Timestamp timeStamp;
    private String priority;
    
    private Date inGateTime;

    public Container(){
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUnitGkey() {
        return unitGkey;
    }

    public void setUnitGkey(int unitGkey) {
        this.unitGkey = unitGkey;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public int getLengthType() {
        return lengthType;
    }

    public void setLengthType(int lengthType) {
        this.lengthType = lengthType;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public ReeferContainer getReefer() {
        return reefer;
    }

    public void setReefer(ReeferContainer reefer) {
        this.reefer = reefer;
    }

    public boolean isTank() {
        return tank;
    }

    public void setTank(boolean tank) {
        this.tank = tank;
    }

    public boolean isFlatRack() {
        return flatRack;
    }

    public void setFlatRack(boolean flatRack) {
        this.flatRack = flatRack;
    }

    public boolean isOpenTop() {
        return openTop;
    }

    public void setOpenTop(boolean openTop) {
        this.openTop = openTop;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public boolean isPairable() {
        return pairable;
    }

    public void setPairable(boolean pairable) {
        this.pairable = pairable;
    }

    public Integer getPairedWith() {
        return pairedWith;
    }

    public void setPairedWith(Integer pairedWith) {
        this.pairedWith = pairedWith;
    }

    public float getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(float grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public Hazards getHazardous() {
        return hazardous;
    }

    public void setHazardous(Hazards hazardous) {
        this.hazardous = hazardous;
    }

    public boolean isOog() {
        return oog;
    }

    public void setOog(boolean oog) {
        this.oog = oog;
    }

    public Holds getHolds() {
        return holds;
    }

    public void setHolds(Holds holds) {
        this.holds = holds;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public CarrierType getObCarrierType() {
        return obCarrierType;
    }

    public void setObCarrierType(CarrierType obCarrierType) {
        this.obCarrierType = obCarrierType;
    }

    public String getObCarrierRef() {
        return obCarrierRef;
    }

    public void setObCarrierRef(String obCarrierRef) {
        this.obCarrierRef = obCarrierRef;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLineOp() {
        return lineOp;
    }

    public void setLineOp(String lineOp) {
        this.lineOp = lineOp;
    }

    public String getSpcialStow1() {
        return specialStow1;
    }

    public void setSpcialStow1(String spcialStow1) {
        this.specialStow1 = spcialStow1;
    }

    public Timestamp getTimestamp() {
        return timeStamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timeStamp = timestamp;
    }

    public Integer getWiGkey() {
        return wiGkey;
    }

    public void setWiGkey(Integer wiGkey) {
        this.wiGkey = wiGkey;
    }

    public Date getInGateTime() {
        return inGateTime;
    }

    public void setInGateTime(Date inGateTime) {
        this.inGateTime = inGateTime;
    }

    public String getSpecialStow1() {
        return specialStow1;
    }

    public void setSpecialStow1(String specialStow1) {
        this.specialStow1 = specialStow1;
    }
}
