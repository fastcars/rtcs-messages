package com.patrick.n4.rtcsmsgs.common;

import java.util.List;


public class ContainerBody {

    private List<Container> ctrCurrent;
    private List<Container> ctrPending;
    private List<Container> ctrUpdate;

    public ContainerBody(){

    }

    public List<Container> getCtrCurrent() {
        return ctrCurrent;
    }

    public void setCtrCurrent(List<Container> ctrCurrent) {
        this.ctrCurrent = ctrCurrent;
    }

    public List<Container> getCtrPending() {
        return ctrPending;
    }

    public void setCtrPending(List<Container> ctrPending) {
        this.ctrPending = ctrPending;
    }

    public List<Container> getCtrUpdate() {
        return ctrUpdate;
    }

    public void setCtrUpdate(List<Container> ctrUpdate) {
        this.ctrUpdate = ctrUpdate;
    }
}
