package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class CraneAction {

    @XStreamAsAttribute
    private String craneId;
    @XStreamAsAttribute
    private String action;

    public CraneAction(){}

    public CraneAction(String crane, String action){
        this.action =action;
        this.craneId = crane;
    }

    public String getCraneId() {
        return craneId;
    }

    public void setCraneId(String craneId) {
        this.craneId = craneId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
