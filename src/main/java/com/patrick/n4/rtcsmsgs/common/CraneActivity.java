package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class CraneActivity {
    @XStreamAsAttribute
    private int craneActiveGkey;
    @XStreamAsAttribute
    private CraneActivityType type;
    @XStreamAsAttribute
    private Positioning positioning;
    @XStreamAsAttribute
    private int duration;


    public CraneActivity(){}

    public int getCraneActiveGkey() {
        return craneActiveGkey;
    }

    public void setCraneActiveGkey(int craneActiveGkey) {
        this.craneActiveGkey = craneActiveGkey;
    }

    public CraneActivityType getType() {
        return type;
    }

    public void setType(CraneActivityType type) {
        this.type = type;
    }

    public Positioning getPositioning() {
        return positioning;
    }

    public void setPositioning(Positioning positioning) {
        this.positioning = positioning;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
