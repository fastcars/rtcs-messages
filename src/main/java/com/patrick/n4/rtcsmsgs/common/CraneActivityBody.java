package com.patrick.n4.rtcsmsgs.common;

public class CraneActivityBody {
    private CraneAction craneAction;
    private Integer unitGkey;
    private Integer twinnedWithWiGkey;
    private Position  position;
    private Integer craneActivGkey;


    public   CraneActivityBody(){
        this.craneAction = new CraneAction();
    }

    public CraneAction getCraneAction() {
        return craneAction;
    }

    public void setCraneAction(CraneAction craneAction) {
        this.craneAction = craneAction;
    }

    public int getUnitGkey() {
        return unitGkey;
    }

    public void setUnitGkey(int unitGkey) {
        this.unitGkey = unitGkey;
    }

    public int getTwinnedWithWiGkey() {
        return twinnedWithWiGkey;
    }

    public void setTwinnedWithWiGkey(int twinnedWithWiGkey) {
        this.twinnedWithWiGkey = twinnedWithWiGkey;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getCraneActivGkey() {
        return craneActivGkey;
    }

    public void setCraneActivGkey(Integer craneActivGkey) {
        this.craneActivGkey = craneActivGkey;
    }
}
