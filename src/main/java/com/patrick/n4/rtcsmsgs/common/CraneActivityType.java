package com.patrick.n4.rtcsmsgs.common;

public enum CraneActivityType {
    BINS,
    HATCH
}
