package com.patrick.n4.rtcsmsgs.common;

public class ExchangeLaneBody {
    private ExchangeLaneBodyContent exchangeLane;

    public ExchangeLaneBody(){
        this.exchangeLane = new ExchangeLaneBodyContent();
    }

    public ExchangeLaneBodyContent getExchangeLane() {
        return exchangeLane;
    }

    public void setExchangeLane(ExchangeLaneBodyContent exchangeLane) {
        this.exchangeLane = exchangeLane;
    }


    public void setAction(String action){
        this.exchangeLane.setAction(action);
    }

    public void setMsic(String msic){
        this.exchangeLane.setMsisc(msic);
    }

    public void setLane(String lane){
        this.exchangeLane.setLane(lane);
    }
}
