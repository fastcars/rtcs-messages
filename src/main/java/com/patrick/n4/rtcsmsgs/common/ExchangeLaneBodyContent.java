package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class ExchangeLaneBodyContent {
    @XStreamAsAttribute
    private String action;
    @XStreamAsAttribute
    private String msisc;
    @XStreamAsAttribute
    private String lane;


    public ExchangeLaneBodyContent(){
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMsisc() {
        return msisc;
    }

    public void setMsisc(String msisc) {
        this.msisc = msisc;
    }

    public String getLane() {
        return lane;
    }

    public void setLane(String lane) {
        this.lane = lane;
    }
}
