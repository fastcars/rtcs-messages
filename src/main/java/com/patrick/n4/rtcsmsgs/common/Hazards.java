package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("hazardous")
public class Hazards {
    @XStreamImplicit(itemFieldName = "imdg")
    private List<String> imdg;

    public Hazards(){}

    public List<String> getImdg() {
        return imdg;
    }

    public void setImdg(List<String> imdg) {
        this.imdg = imdg;
    }
}
