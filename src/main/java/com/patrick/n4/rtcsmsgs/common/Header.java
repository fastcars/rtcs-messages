package com.patrick.n4.rtcsmsgs.common;


import java.sql.Timestamp;

/**
 *
 *
         <header>
             <senderId>RTCS</senderId>
             <receiverId>N4</receiverId>
             <msgType>startUp</msgType>
             <msgId>55678</msgId>
             <timeStamp>2017-11-21T11:32:25.719</timeStamp>
         </header>
 *
 *
 */


public class Header {
    private String senderId;
    private String receiverId;
    private String msgType;
    private int msgId;
    private Timestamp timeStamp;

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }
}
