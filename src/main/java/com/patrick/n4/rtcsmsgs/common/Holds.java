package com.patrick.n4.rtcsmsgs.common;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("hazardous")
public class Holds {
    @XStreamImplicit(itemFieldName = "hold")
    private List<String> hold;


    public Holds(){}

    public List<String> getHold() {
        return hold;
    }

    public void setHold(List<String> hold) {
        this.hold = hold;
    }
}
