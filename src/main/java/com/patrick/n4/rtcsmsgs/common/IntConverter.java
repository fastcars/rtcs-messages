package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.converters.*;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


public class IntConverter extends AbstractSingleValueConverter {

    //implements Converter {

    public boolean canConvert(Class type) {
        return type.equals(int.class) || type.equals(Integer.class) ;
    }

    @Override
    public Object fromString(String s) {
        if(s==null || s.length()==0 || s.trim().length()==0)
            return null;
        return new Integer(s);
    }

    /*
    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
        if(value instanceof String){
            String val = (String) value;
            if(val.length()==0 || val.trim().length()==0){
                writer.setValue("");
            }
        } else {
            writer.setValue(""+value);
        }

    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        String value = reader.getValue();
        if(value==null || value.length()==0 || value.trim().length()==0){
            return new Integer(0);
        }
        return new Integer(value);
    } */

}