package com.patrick.n4.rtcsmsgs.common;

public enum LiftType {
    SINGLE,
    DOUBLE;
}
