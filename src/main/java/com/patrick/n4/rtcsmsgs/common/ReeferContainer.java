package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

public class ReeferContainer {
    private Integer reqTemp;
    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"yes", "no"})
    private boolean wantsPower;
    @XStreamConverter(value=BooleanConverter.class, booleans={false}, strings={"yes", "no"})
    private boolean  onPower;

    public ReeferContainer(){

    }

    public Integer getReqTemp() {
        return reqTemp;
    }

    public void setReqTemp(Integer reqTemp) {
        this.reqTemp = reqTemp;
    }

    public boolean getWantsPower() {
        return wantsPower;
    }

    public void setWantsPower(boolean wantsPower) {
        this.wantsPower = wantsPower;
    }

    public boolean  getOnPower() {
        return onPower;
    }

    public void setOnPower(boolean  onPower) {
        this.onPower = onPower;
    }
}
