package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;


public  class StartupBody {
    @XStreamAlias("startUp")
    private StartupBodyContent content;

    public StartupBody(){
        this.content = new StartupBodyContent();
    }

    public StartupBody(StartupBodyContent action){
        this.content = action;
    }


    public StartupBodyContent getContent() {
        return content;
    }

    public void setContent(StartupBodyContent content) {
        this.content = content;
    }
}
