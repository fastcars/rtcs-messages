package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class StartupBodyContent {
    @XStreamAsAttribute
    private String action;


    public StartupBodyContent(){
        this.action = "NORMAL";
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
