package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class StraddleAction {
    @XStreamAsAttribute
    private String action;

    @XStreamAsAttribute
    private String straddleId;

    public StraddleAction(){}

    public StraddleAction(String action, String straddleId){
        this.action = action;
        this.straddleId = straddleId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStraddleId() {
        return straddleId;
    }

    public void setStraddleId(String straddleId) {
        this.straddleId = straddleId;
    }
}
