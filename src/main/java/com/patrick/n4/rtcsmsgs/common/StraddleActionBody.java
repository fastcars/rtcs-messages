package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class StraddleActionBody {
    private StraddleAction straddleAction;
    @XStreamAlias("wiGkey")
    private Integer  workInstructionGKey;

    private int unitGkey;
    private int twinnedWithGkey;
    private Position position;


    public StraddleActionBody(){
        this.straddleAction = new StraddleAction();
    }

    public StraddleAction getStraddleAction() {
        return straddleAction;
    }

    public void setStraddleAction(StraddleAction straddleAction) {
        this.straddleAction = straddleAction;
    }

    public int getWorkInstructionGKey() {
        return workInstructionGKey;
    }

    public void setWorkInstructionGKey(int workInstructionGKey) {
        this.workInstructionGKey = workInstructionGKey;
    }

    public int getUnitGkey() {
        return unitGkey;
    }

    public void setUnitGkey(int unitGkey) {
        this.unitGkey = unitGkey;
    }

    public int getTwinnedWithGkey() {
        return twinnedWithGkey;
    }

    public void setTwinnedWithGkey(int twinnedWithGkey) {
        this.twinnedWithGkey = twinnedWithGkey;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
