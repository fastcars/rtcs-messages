package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimestampConverter implements Converter {

    private SimpleDateFormat formatter = new SimpleDateFormat(
            "yyyy-mm-dd'T'HH:mm:ss.SSS");

    public boolean canConvert(Class clazz) {
        // This converter is only for Calendar fields.
        return Timestamp.class.isAssignableFrom(clazz);
        //return Calendar.class.isAssignableFrom(clazz);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
        Timestamp calendar = (Timestamp) value;
        
        writer.setValue(formatter.format(calendar));
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        GregorianCalendar calendar = new GregorianCalendar();
        try {
            calendar.setTime(formatter.parse(reader.getValue()));
        } catch (ParseException e) {
            throw new ConversionException(e.getMessage(), e);
        }
        return new Timestamp(calendar.getTimeInMillis());
    }
}