package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class Truck {
    @XStreamAsAttribute
    private String id;
    @XStreamAsAttribute
    private String misc;
    @XStreamAsAttribute
    private String chsProfile;

    public Truck(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public String getChsProfile() {
        return chsProfile;
    }

    public void setChsProfile(String chsProfile) {
        this.chsProfile = chsProfile;
    }
}
