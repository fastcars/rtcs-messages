package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.Date;
import java.util.List;

@XStreamAlias("trkAppt")
public class TruckActiivityItem {

    private Truck trk;
    @XStreamImplicit
    private List<TruckContainer> ctrs;

    private Date timeSlot;


    public TruckActiivityItem(){
        this.trk = new Truck();
    }

    public Truck getTrk() {
        return trk;
    }

    public void setTrk(Truck trk) {
        this.trk = trk;
    }

    public List<TruckContainer> getCtrs() {
        return ctrs;
    }

    public void setCtrs(List<TruckContainer> ctrs) {
        this.ctrs = ctrs;
    }

    public Date getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Date timeSlot) {
        this.timeSlot = timeSlot;
    }
}
