package com.patrick.n4.rtcsmsgs.common;

import java.util.List;

public class TruckActivityBody {
    private List<TruckActiivityItem> trkPending;
    private List<TruckActiivityItem> trkCurrent;
    private List<TruckActiivityItem> trkImminent;


    public TruckActivityBody(){}

    public List<TruckActiivityItem> getTrkPending() {
        return trkPending;
    }

    public void setTrkPending(List<TruckActiivityItem> trkPending) {
        this.trkPending = trkPending;
    }

    public List<TruckActiivityItem> getTrkCurrent() {
        return trkCurrent;
    }

    public void setTrkCurrent(List<TruckActiivityItem> trkCurrent) {
        this.trkCurrent = trkCurrent;
    }

    public List<TruckActiivityItem> getTrkImminent() {
        return trkImminent;
    }

    public void setTrkImminent(List<TruckActiivityItem> trkImminent) {
        this.trkImminent = trkImminent;
    }
}
