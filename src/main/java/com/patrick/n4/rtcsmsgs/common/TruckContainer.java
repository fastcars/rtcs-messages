package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


@XStreamAlias("ctr")
public class TruckContainer {
    @XStreamAsAttribute
    private String id;

    @XStreamAsAttribute
    protected Integer  unitGkey;

    @XStreamAsAttribute
    private String tranType;
    
    @XStreamAsAttribute
    private String iso;

    public TruckContainer(){}


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUnitGkey() {
        return unitGkey;
    }

    public void setUnitGkey(Integer unitGkey) {
        this.unitGkey = unitGkey;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }
}
