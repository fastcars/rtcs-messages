package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.sql.Timestamp;

@XStreamAlias("vv")
public class VesselVisit {

    @XStreamAlias("vvRef")
    private String vesselRef;
    private Timestamp eta;
    private Timestamp etd;
    private Integer berth;
    private String status;

    public VesselVisit (){}

    public String getVesselRef() {
        return vesselRef;
    }

    public void setVesselRef(String vesselRef) {
        this.vesselRef = vesselRef;
    }

    public Timestamp getEta() {
        return eta;
    }

    public void setEta(Timestamp eta) {
        this.eta = eta;
    }

    public Timestamp getEtd() {
        return etd;
    }

    public void setEtd(Timestamp etd) {
        this.etd = etd;
    }

    public Integer getBerth() {
        return berth;
    }

    public void setBerth(Integer berth) {
        this.berth = berth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
