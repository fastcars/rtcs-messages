package com.patrick.n4.rtcsmsgs.common;

import java.util.ArrayList;
import java.util.List;

public class VesselVisitBody {
    private List<VesselVisit> vvPending;
    private List<VesselVisit> vvCurrent;
    private List<VesselVisit> vvDeparted;


    public VesselVisitBody(){}

    public List<VesselVisit> getVvPending() {
        return vvPending;
    }

    public void setVvPending(List<VesselVisit> vvPending) {
        this.vvPending = vvPending;
    }

    public List<VesselVisit> getVvCurrent() {
        return vvCurrent;
    }

    public void setVvCurrent(List<VesselVisit> vvCurrent) {
        this.vvCurrent = vvCurrent;
    }

    public List<VesselVisit> getVvDeparted() {
        return vvDeparted;
    }

    public void setVvDeparted(List<VesselVisit> vvDeparted) {
        this.vvDeparted = vvDeparted;
    }
}
