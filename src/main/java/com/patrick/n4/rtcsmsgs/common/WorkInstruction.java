package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class WorkInstruction {

    @XStreamAsAttribute
    private int sequence;
    @XStreamAsAttribute
    private String moveKind;
    @XStreamAsAttribute
    private String doorDirection;
    @XStreamAsAttribute
    private LiftType liftType;
    @XStreamAsAttribute
    private int unitGkey;
    @XStreamAsAttribute
    private Integer associatedUnitGkey;
    private Position plannedPosition;
    

    public WorkInstruction(){}

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getMoveKind() {
        return moveKind;
    }

    public void setMoveKind(String moveKind) {
        this.moveKind = moveKind;
    }

    public String getDoorDirection() {
        return doorDirection;
    }

    public void setDoorDirection(String doorDirection) {
        this.doorDirection = doorDirection;
    }

    public LiftType getLiftType() {
        return liftType;
    }

    public void setLiftType(LiftType liftType) {
        this.liftType = liftType;
    }

    public int getUnitGkey() {
        return unitGkey;
    }

    public void setUnitGkey(int unitGkey) {
        this.unitGkey = unitGkey;
    }

    public Integer getAssociatedUnitGkey() {
        return associatedUnitGkey;
    }

    public void setAssociatedUnitGkey(Integer associatedUnitGkey) {
        this.associatedUnitGkey = associatedUnitGkey;
    }

    public Position getPlannedPosition() {
        return plannedPosition;
    }

    public void setPlannedPosition(Position plannedPosition) {
        this.plannedPosition = plannedPosition;
    }


}
