package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

@XStreamAlias("body")
public class WorkQueueBody {
    private List<WorkQueueItem> wqCurrent;


    public WorkQueueBody(){}

    public List<WorkQueueItem> getWqCurrent() {
        return wqCurrent;
    }

    public void setWqCurrent(List<WorkQueueItem> wqCurrent) {
        this.wqCurrent = wqCurrent;
    }
}
