package com.patrick.n4.rtcsmsgs.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("workQueue")
public class WorkQueueItem {
    @XStreamAsAttribute
    private String name;
    @XStreamAsAttribute
    private String workOrder;
    @XStreamAsAttribute
    private String craneId;
    @XStreamAsAttribute
    @XStreamAlias("vvRef")
    private String VesselRef;
    @XStreamAsAttribute
    private  String deck;
    @XStreamAsAttribute
    private String row;
    @XStreamAsAttribute
    private String dualCycleWq;

    private List<CraneActivity> craneActivities;
    @XStreamImplicit(itemFieldName = "workInstruction")
    private List<WorkInstruction> workInstruction;


    public WorkQueueItem(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(String workOrder) {
        this.workOrder = workOrder;
    }

    public String getCraneId() {
        return craneId;
    }

    public void setCraneId(String craneId) {
        this.craneId = craneId;
    }

    public String getVesselRef() {
        return VesselRef;
    }

    public void setVesselRef(String vesselRef) {
        VesselRef = vesselRef;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getDualCycleWq() {
        return dualCycleWq;
    }

    public void setDualCycleWq(String dualCycleWq) {
        this.dualCycleWq = dualCycleWq;
    }

    public List<CraneActivity> getCraneActivities() {
        return craneActivities;
    }

    public void setCraneActivities(List<CraneActivity> craneActivities) {
        this.craneActivities = craneActivities;
    }

    public List<WorkInstruction> getWorkInstruction() {
        return workInstruction;
    }

    public void setWorkInstruction(List<WorkInstruction> workInstruction) {
        this.workInstruction = workInstruction;
    }
}
