package com.patrick.n4.rtcsmsgs.common;


import com.patrick.n4.rtcsmsgs.fromrtcs.CraneActivity;
import com.patrick.n4.rtcsmsgs.fromrtcs.RTCSStartup;
import com.patrick.n4.rtcsmsgs.fromrtcs.WorkOrder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XmlStreamUtil {

    private XmlStreamUtil(){}


    /**
     *
     * Make conistently
     * @return
     */
    public static XStream MakeXStream(){

        XStream xstream = new XStream(new DomDriver());
        xstream.autodetectAnnotations(true);

        xstream.alias("RTCSStraddleAction", WorkOrder.class);
        xstream.alias("RTCSCraneAction", CraneActivity.class);
        xstream.alias("RTCSStartup", RTCSStartup.class);
        xstream.registerConverter(new TimestampConverter());
        xstream.registerConverter(new IntConverter());
        xstream.registerConverter(new DateConverter("yyyy-MM-dd HH:mm", new String [] {}, true));
        return xstream;
    }

}
