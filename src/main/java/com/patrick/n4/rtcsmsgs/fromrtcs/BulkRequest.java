package com.patrick.n4.rtcsmsgs.fromrtcs;


import com.patrick.n4.rtcsmsgs.common.BulkRequestBody;
import com.patrick.n4.rtcsmsgs.common.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RTCSBulkRequest")
public class BulkRequest {
    private Header header;
    private BulkRequestBody body;


    public BulkRequest(){
        this.header = new Header();
        this.body = new BulkRequestBody();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public BulkRequestBody getBody() {
        return body;
    }

    public void setBody(BulkRequestBody body) {
        this.body = body;
    }
}
