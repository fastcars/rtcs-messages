package com.patrick.n4.rtcsmsgs.fromrtcs;

import com.patrick.n4.rtcsmsgs.common.CraneActivityBody;
import com.patrick.n4.rtcsmsgs.common.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RTCSCraneAction")
public class CraneActivity {
    private Header header;
    private CraneActivityBody body;

    public CraneActivity(){
        this.body = new CraneActivityBody();
        this.header = new Header();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public CraneActivityBody getBody() {
        return body;
    }

    public void setBody(CraneActivityBody body) {
        this.body = body;
    }
}
