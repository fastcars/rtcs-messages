package com.patrick.n4.rtcsmsgs.fromrtcs;

import com.patrick.n4.rtcsmsgs.common.ExchangeLaneBody;
import com.patrick.n4.rtcsmsgs.common.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("RTCSExchangeLane")
public class ExchangeLane {
    private Header header;
    private ExchangeLaneBody body;

    public ExchangeLane(){
        this.header = new Header();
        this.body = new ExchangeLaneBody();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public ExchangeLaneBody getBody() {
        return body;
    }

    public void setBody(ExchangeLaneBody body) {
        this.body = body;
    }
}
