package com.patrick.n4.rtcsmsgs.fromrtcs;

import com.patrick.n4.rtcsmsgs.common.StartupBody;
import com.patrick.n4.rtcsmsgs.common.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RTCSStartup")
public class RTCSStartup {
    
    Header header;
    StartupBody body;

    public RTCSStartup(){
        this.body =new StartupBody();
        this.header = new Header();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public StartupBody getStartupBody() {
        return body;
    }

    public void setStartupBody(StartupBody startupBody) {
        this.body = startupBody;
    }
}
