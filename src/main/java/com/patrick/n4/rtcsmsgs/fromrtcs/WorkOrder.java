package com.patrick.n4.rtcsmsgs.fromrtcs;

import com.patrick.n4.rtcsmsgs.common.Header;
import com.patrick.n4.rtcsmsgs.common.StraddleActionBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("RTCStraddleAction")
public class WorkOrder {
    private Header header;
    private StraddleActionBody body;


    public WorkOrder(){
        this.header = new Header();
        this.body = new StraddleActionBody();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public StraddleActionBody getBody() {
        return body;
    }

    public void setBody(StraddleActionBody body) {
        this.body = body;
    }
}
