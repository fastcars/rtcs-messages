package com.patrick.n4.rtcsmsgs.tortcs;

import com.patrick.n4.rtcsmsgs.common.ContainerBody;
import com.patrick.n4.rtcsmsgs.common.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("N4CTRUpdates")
public class ContainerActivity {
    private Header header;
    private ContainerBody body;


    public ContainerActivity(){}

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public ContainerBody getBody() {
        return body;
    }

    public void setBody(ContainerBody body) {
        this.body = body;
    }
}
