package com.patrick.n4.rtcsmsgs.tortcs;

import com.patrick.n4.rtcsmsgs.common.Header;
import com.patrick.n4.rtcsmsgs.common.StartupBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("N4Startup")
public class N4Startup {

    @XStreamAsAttribute
    @XStreamAlias("xmlns:xsi")
    final String xsi = "http://www.w3.org/2001/XMLSchema-instance";

    private Header header;
    private StartupBody body;


    public N4Startup(){
        this.header = new Header();
        this.body = new StartupBody();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public StartupBody getBody() {
        return body;
    }

    public void setBody(StartupBody body) {
        this.body = body;
    }
}
