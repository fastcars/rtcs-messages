package com.patrick.n4.rtcsmsgs.tortcs;


import com.patrick.n4.rtcsmsgs.common.Header;
import com.patrick.n4.rtcsmsgs.common.TruckActivityBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("N4TRKUpdates")
public class TruckActivity {
    private Header header;
    private TruckActivityBody body;


    public TruckActivity(){
        this.header = new Header();
        this.body = new TruckActivityBody();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public TruckActivityBody getBody() {
        return body;
    }

    public void setBody(TruckActivityBody body) {
        this.body = body;
    }
}
