package com.patrick.n4.rtcsmsgs.tortcs;

import com.patrick.n4.rtcsmsgs.common.Header;
import com.patrick.n4.rtcsmsgs.common.VesselVisitBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("N4VVUpdates")
public class VesselVisitActivity {
    private Header header;
    private VesselVisitBody body;


    public VesselVisitActivity(){
        this.header = new Header();
        this.body = new VesselVisitBody();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public VesselVisitBody getBody() {
        return body;
    }

    public void setBody(VesselVisitBody body) {
        this.body = body;
    }
}
