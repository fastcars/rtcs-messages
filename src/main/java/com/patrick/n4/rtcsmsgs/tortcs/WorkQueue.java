package com.patrick.n4.rtcsmsgs.tortcs;

import com.patrick.n4.rtcsmsgs.common.Header;
import com.patrick.n4.rtcsmsgs.common.WorkQueueBody;

public class WorkQueue {
    private Header header;
    private WorkQueueBody body;

    public WorkQueue(){}

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public WorkQueueBody getBody() {
        return body;
    }

    public void setBody(WorkQueueBody body) {
        this.body = body;
    }
}
