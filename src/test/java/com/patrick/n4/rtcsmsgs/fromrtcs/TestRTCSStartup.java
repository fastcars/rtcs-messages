package com.patrick.n4.rtcsmsgs.fromrtcs;

import com.patrick.n4.rtcsmsgs.common.*;
import com.patrick.n4.rtcsmsgs.tortcs.*;
import com.thoughtworks.xstream.XStream;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

public class TestRTCSStartup {


    @Test
    public void testSerializeStartup(){
        RTCSStartup st = new RTCSStartup();
        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("startUP");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");
        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));

    }

    @Test
    public void testSerializeBulkRequest(){
        BulkRequest st = new BulkRequest();
        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");
        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));

    }



    @Test
    public void testSerializeExchangeLane(){
        ExchangeLane st = new ExchangeLane();
        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");
        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));
        ExchangeLaneBodyContent elc = new ExchangeLaneBodyContent();
        elc.setLane("13");
        elc.setAction("SWIPE");
        elc.setMsisc("1212121");
        st.getBody().setExchangeLane(elc);

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));

    }


    @Test
    public void testSerializeWorkOrder(){
        WorkOrder st = new WorkOrder();
        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");
        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        st.getBody().setStraddleAction(new StraddleAction("ASSIGN", "445"));
        st.getBody().setUnitGkey(1212);
        st.getBody().setWorkInstructionGKey(45646);
        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));
    }


    @Test
    public void testSerializeWorkOrderWithPosition(){
        WorkOrder st = new WorkOrder();
        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");
        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        st.getBody().setStraddleAction(new StraddleAction("ASSIGN", "445"));
        st.getBody().setUnitGkey(1212);
        st.getBody().setWorkInstructionGKey(45646);

        Position pos = new Position();
        pos.setBlock("B");
        pos.setRow("12");
        pos.setSlot("S");
        pos.setTier("2");
        st.getBody().setPosition(pos);

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));
    }




    private String getFile(String fileName) {

        StringBuilder result = new StringBuilder();

        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }



    @Test
    public void deserializeISOCheck(){
        String xml = getFile("RTCSUnitISOCheck.xml");

        final XStream xStream = XmlStreamUtil.MakeXStream();
        WorkOrder wi = (WorkOrder) xStream.fromXML(xml);
        assertTrue(wi.getBody().getStraddleAction().getAction().equals("ISOCHECK"));
        assertTrue(wi.getBody().getPosition().getBlock().equals("B"));
    }


    @Test
    public void deserializeWIList(){
        String xml = getFile("RTCSWILift.xml");

        final XStream xStream = XmlStreamUtil.MakeXStream();
        WorkOrder wi = (WorkOrder) xStream.fromXML(xml);
        assertTrue(wi.getBody().getStraddleAction().getAction().equals("LIFT"));
        assertTrue(wi.getBody().getUnitGkey()==881910);
    }

    @Test
    public void deserializeCraneLift(){
        String xml = getFile("RTCSCraneLift.xml");

        final XStream xStream = XmlStreamUtil.MakeXStream();
        CraneActivity ca = (CraneActivity) xStream.fromXML(xml);
        assertTrue(ca.getBody().getCraneAction().getAction().equals("LIFT"));
        assertTrue(ca.getBody().getUnitGkey()==881910);
        assertTrue(ca.getBody().getTwinnedWithWiGkey()==22677);
    }



    @Test
    public void deserializeCraneDelay(){
        String xml = getFile("RTCSCraneDelay.xml");

        final XStream xStream = XmlStreamUtil.MakeXStream();
        CraneActivity ca = (CraneActivity) xStream.fromXML(xml);
        assertTrue(ca.getBody().getCraneAction().getAction().equals("DELAYSTART"));
        
    }



    @Test
    public void testN4StartupSerialize(){
        N4Startup st = new N4Startup();
        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");
        
        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        StartupBodyContent sbc = new StartupBodyContent();
        sbc.setAction("READY");
        st.getBody().setContent(sbc);

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));
    }


    @Test
    public void testVesselVisitSerialize(){
        VesselVisitActivity st = new VesselVisitActivity();

        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");

        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        List<VesselVisit> pend = new ArrayList<VesselVisit>();
        VesselVisit vv = new VesselVisit();
        vv.setBerth(3);
        vv.setEta(new Timestamp((new Date()).getTime()));
        vv.setEtd(new Timestamp((new Date()).getTime()));
        vv.setStatus("INBOUND");
        vv.setVesselRef("HCF651");
        pend.add(vv);


        vv = new VesselVisit();
        vv.setBerth(1);
        vv.setEta(new Timestamp((new Date()).getTime()));
        vv.setEtd(new Timestamp((new Date()).getTime()));
        vv.setStatus("WORKING");
        vv.setVesselRef("KJA888");
        pend.add(vv);
        st.getBody().setVvPending(pend);

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));

    }

    @Test
    public void testVesselVisitSerializeCurrent(){
        VesselVisitActivity st = new VesselVisitActivity();

        st.getHeader().setMsgId(10101);
        st.getHeader().setMsgType("bulkRequest");
        st.getHeader().setReceiverId("N4");
        st.getHeader().setSenderId("RTCS");

        st.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        List<VesselVisit> pend = new ArrayList<VesselVisit>();
        VesselVisit vv = new VesselVisit();
        vv.setBerth(3);
        vv.setEta(new Timestamp((new Date()).getTime()));
        vv.setEtd(new Timestamp((new Date()).getTime()));
        vv.setStatus("INBOUND");
        vv.setVesselRef("HCF651");
        pend.add(vv);


        vv = new VesselVisit();
        vv.setBerth(1);
        vv.setEta(new Timestamp((new Date()).getTime()));
        vv.setEtd(new Timestamp((new Date()).getTime()));
        vv.setStatus("WORKING");
        vv.setVesselRef("KJA888");
        pend.add(vv);
        st.getBody().setVvCurrent(pend);

        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(st));

    }


    @Test
    public void testSerializeTruckActivity(){
        TruckActivity ta = new TruckActivity();

        ta.getHeader().setMsgId(10101);
        ta.getHeader().setMsgType("bulkRequest");
        ta.getHeader().setReceiverId("N4");
        ta.getHeader().setSenderId("RTCS");

        ta.getHeader().setTimeStamp(new Timestamp((new Date()).getTime()));

        TruckActivityBody tab = ta.getBody();
        List<TruckActiivityItem>   items = new ArrayList<>();
        TruckActiivityItem it = new TruckActiivityItem();
        List<TruckContainer> ctrs = new ArrayList<>();
        TruckContainer tc = new TruckContainer();
        tc.setId("MSKU12121111");
        tc.setIso("2200");
        tc.setTranType("RI");
        tc.setUnitGkey(232323);
        ctrs.add(tc);
        tc = new TruckContainer();
        tc.setId("MKHA2323222");
        tc.setIso("45G1");
        tc.setTranType("DI");
        tc.setUnitGkey(91323);
        ctrs.add(tc);

        Truck tr = new Truck();
        tr.setChsProfile("10W");
        tr.setId("TR010");
        tr.setMisc("121211");
        it.setTrk(tr);

        it.setTimeSlot(new Date());
        it.setCtrs(ctrs);
        items.add(it);
        tab.setTrkPending(items);


        final XStream xStream = XmlStreamUtil.MakeXStream();
        System.out.println(xStream.toXML(ta));
    }


    @Test
    public void testDeserializeContainerMsg(){
        String xml = getFile("RTCSContainer.xml");

        final XStream xstream = XmlStreamUtil.MakeXStream();
        xstream.alias("N4CTRUpdates", ContainerActivity.class);
        ContainerActivity ca = (ContainerActivity) xstream.fromXML(xml);
        assertTrue(ca.getBody().getCtrCurrent().get(0).getCommodity().equals("HIDES"));
    }


    @Test
    public void testDeserializeWorkQueue(){
        String xml = getFile("RTCSWorkQueue.xml");

        final XStream xstream = XmlStreamUtil.MakeXStream();
        xstream.alias("N4WQUpdates", WorkQueue.class);
        WorkQueue ca = (WorkQueue) xstream.fromXML(xml);
        assertTrue(ca.getBody().getWqCurrent().get(0).getWorkInstruction().get(1).getSequence()==3);
    }

    
}
